﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(marital.Startup))]
namespace marital
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
